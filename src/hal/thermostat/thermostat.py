from abc import ABC, abstractmethod

from ...domain.state.state import State


class Thermostat(State, ABC):
    def __init__(self, state_id: int, temperature: State):
        super().__init__(state_id)
        self._temp = temperature

    @abstractmethod
    async def run(self):
        pass

    @abstractmethod
    def set_value(self, value: float):
        pass
