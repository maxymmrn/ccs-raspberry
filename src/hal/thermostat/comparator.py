import asyncio
import RPi.GPIO as GPIO
from .thermostat import Thermostat
from ...domain.state.state import State
from ...domain.state.type import StateType


class Comparator(Thermostat):
    _HYSTERESIS = 0.5
    __slots__ = ('_state_id', '_temp', '_heater_pin', '_cooler_pin', '_value')

    def __init__(self, state_id: int, temperature: State, heater_pin: int, cooler_pin: int):
        super().__init__(state_id, temperature)
        self._heater_pin = heater_pin
        self._cooler_pin = cooler_pin
        self._value = 30.0
        self.__initialized = False
        GPIO.setup(self._heater_pin, GPIO.OUT)
        GPIO.setup(self._cooler_pin, GPIO.OUT)
        asyncio.create_task(self.run())

    async def run(self):
        while True:
            temperature = await self._temp.get_value()
            if temperature > self._value + Comparator._HYSTERESIS:
                GPIO.output(self._heater_pin, GPIO.LOW)
                GPIO.output(self._cooler_pin, GPIO.HIGH)
            elif temperature < self._value - Comparator._HYSTERESIS:
                GPIO.output(self._heater_pin, GPIO.HIGH)
                GPIO.output(self._cooler_pin, GPIO.LOW)
            else:
                GPIO.output(self._heater_pin, GPIO.LOW)
                GPIO.output(self._cooler_pin, GPIO.LOW)
                pass
            await asyncio.sleep(0.1)

    def set_value(self, value: float):
        self._value = value

    def get_type(self) -> StateType:
        return StateType.COMPARATOR

    async def get_value(self):
        value = self._value if self.__initialized else 0
        self.__initialized = True
        return value
