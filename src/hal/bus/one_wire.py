import os
import glob
from typing import List

from ...domain.state.type import StateType
from .bus import Bus


class OneWire(Bus):
    _BASE_DIR = '/sys/bus/w1/devices'
    __slots__ = ('_state_id', '_sensor_paths')

    def __init__(self, state_id: int):
        super().__init__(state_id)
        os.system('modprobe w1-gpio')
        os.system('modprobe w1-therm')
        self._sensor_paths = []

    def get_type(self) -> StateType:
        return StateType.ONE_WIRE

    def scan(self) -> List[str]:
        self._sensor_paths = glob.glob(OneWire._BASE_DIR + '/28-*')
        return self._sensor_paths

    async def get_value(self):
        return len(self._sensor_paths)
