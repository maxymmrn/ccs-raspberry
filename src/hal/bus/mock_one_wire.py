from random import randint
from typing import List
from .one_wire import OneWire


# noinspection PyMissingConstructor
class MockOneWire(OneWire):
    __slots__ = ('_state_id', '_sensor_paths')

    def __init__(self, state_id: int):
        self._state_id = state_id
        self._sensor_paths = []

    def scan(self) -> List[str]:
        self._sensor_paths = [f'28-{i}' for i in range(randint(5, 15))]
        return self._sensor_paths
