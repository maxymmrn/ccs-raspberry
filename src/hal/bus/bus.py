from abc import abstractmethod
from typing import List

from ...domain.state.state import State


class Bus(State):

    @abstractmethod
    def scan(self) -> List[str]:
        pass

    def __len__(self):
        return self.get_value()
