from ...domain.state.state import State
from ...domain.state.type import StateType
from aiofile import async_open


class TemperatureSensor(State):
    __slots__ = ('_state_id', '_address')

    def __init__(self, state_id: int, address: str):
        super().__init__(state_id)
        self._address = address

    def get_type(self) -> StateType:
        return StateType.TEMPERATURE

    async def get_value(self) -> float:
        async with async_open(self._get_filename(), 'r') as f:
            str_temp = await f.read()
            return float(str_temp) / 1000

    def _get_filename(self):
        return f'{self._address}/temperature'
