import asyncio

from .temperature import TemperatureSensor
from random import random


class MockTemperatureSensor(TemperatureSensor):
    __slots__ = ('_state_id', '_address', '__prev_value')

    def __init__(self, state_id: int, address: str):
        super().__init__(state_id, address)
        self.__prev_value = random() * 50 - 20

    async def get_value(self) -> float:
        await asyncio.sleep(1000)
        self.__prev_value += random() * 0.3 - 0.15
        return self.__prev_value

    def _get_filename(self):
        return f'{self._address}/temperature'
