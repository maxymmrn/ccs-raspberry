import asyncio
import json
import logging
from typing import NoReturn

from .domain.controller.controller import Controller
from .transport.config import TransportConfig
from .transport.transport import Transport


async def main() -> NoReturn:
    logging.basicConfig(level=logging.INFO)
    controller = Controller()
    await controller.init()
    print('Controller init')
    with open('config.json') as file:
        config = TransportConfig(**json.load(file))
        print('parsed config')
    transport = Transport(config=config, controller=controller)
    await transport.start()


if __name__ == '__main__':
    asyncio.run(main())
