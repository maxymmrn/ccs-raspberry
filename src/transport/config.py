from dataclasses import dataclass


@dataclass
class TransportConfig:
    host: str
    port: int
    id: str
    password: str
    tls: bool
    prefetch_count: int = 200

