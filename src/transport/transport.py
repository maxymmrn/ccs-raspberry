import struct
from dataclasses import dataclass

from aio_pika import connect_robust, Message
from aio_pika.abc import AbstractRobustConnection
from aio_pika.patterns import RPC

from ..domain.controller.controller import Controller
from ..transport.config import TransportConfig


@dataclass
class Transport:
    config: TransportConfig
    controller: Controller

    async def start(self):
        print('starting to connect')
        connection: AbstractRobustConnection = await connect_robust(
            host=self.config.host,
            port=self.config.port,
            login=self.config.id,
            password=self.config.password,
            virtualhost='/',
            ssl=self.config.tls
        )
        print('connected')
        async with connection:
            async with await connection.channel(publisher_confirms=False) as channel:
                print('channel initialized')
                exchange = await channel.get_exchange(name='amq.topic', ensure=True)
                print('starting publishing values')
                rpc = await RPC.create(channel)
                await rpc.register(f'{self.config.id}-add_thermostat', self.controller.add_thermostat)
                await rpc.register(f'{self.config.id}-set_value_thermostat', self.controller.set_value)
                async for address, value in self.controller.subscribe():
                    await exchange.publish(
                        Message(body=struct.pack('!f', value)),
                        f'.devices.{self.config.id}.events{address.path}',
                        mandatory=True
                    )
