from abc import ABC, abstractmethod

from .address import StateAddress
from .type import StateType


class State(ABC):
    __slots__ = ('_state_id',)

    def __init__(self, state_id: int):
        self._state_id = state_id

    def get_address(self) -> StateAddress:
        return StateAddress(self._state_id, self.get_type())

    @abstractmethod
    def get_type(self) -> StateType:
        pass

    @abstractmethod
    async def get_value(self):
        pass
