from dataclasses import dataclass

from .type import StateType


@dataclass
class StateAddress:
    state_id: int
    state_type: StateType

    @property
    def path(self):
        return f'.{self.state_type.value}.{self.state_id}'

    def __hash__(self):
        return hash(self.path)

    def __eq__(self, other):
        if isinstance(other, StateAddress):
            return self.path == other.path
        return NotImplemented

    def __ne__(self, other):
        return not (self == other)
