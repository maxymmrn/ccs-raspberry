from enum import Enum, auto


class StateType(Enum):
    UNKNOWN = auto()
    ONE_WIRE = auto()
    TEMPERATURE = auto()
    COMPARATOR = auto()
