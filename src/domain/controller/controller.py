import asyncio
import os
from typing import Dict, Tuple, AsyncIterator, Iterator, Optional
import RPi.GPIO as GPIO

from .state_values_map import StateValuesMap
from ..state.address import StateAddress
from ..state.state import State
from ..state.type import StateType
from ...hal.thermostat.comparator import Comparator

if not os.environ.get('EMULATOR'):
    from ...hal.bus.one_wire import OneWire
    from ...hal.sensor.temperature import TemperatureSensor
else:
    from ...hal.bus.mock_one_wire import MockOneWire as OneWire
    from ...hal.sensor.mock_temperature import MockTemperatureSensor as TemperatureSensor


class Controller:

    def __init__(self, precision: int = 0.1):
        self._states: Dict[StateAddress, State] = {}
        self._values = StateValuesMap()
        self._precision = precision

    async def init(self):
        await self._init_ow()
        self._init_gpio()

    async def _init_ow(self):
        ow = OneWire(state_id=0)
        await self.add_state(ow)
        addresses = ow.scan()
        print(addresses)
        await asyncio.gather(*[
            self.add_state(TemperatureSensor(state_id, address)) for state_id, address in enumerate(addresses)
        ])

    @staticmethod
    def _init_gpio():
        GPIO.setmode(GPIO.BOARD)
        pass

    def get_state_addresses(self):
        return self._states.keys()

    async def add_state(self, state: State):
        print(state.get_address())
        self._states[state.get_address()] = state
        await self.read_value(state.get_address())

    async def add_thermostat(self, *, config: dict):
        comparator = Comparator(
            config['input_id'],
            self._states[StateAddress(config['input_id'], StateType.TEMPERATURE)],
            config['heater'],
            config['cooler'],
        )
        await self.add_state(comparator)

    async def set_value(self, *, state_id: int, value: float):
        self._states[StateAddress(state_id, StateType.COMPARATOR)].set_value(value)

    def get_state(self, address: StateAddress):
        return self._states[address]

    async def read_value(self, address: StateAddress) -> float:
        value = await self._states[address].get_value()
        self._values.set_value(address, value)
        return value

    async def read_value_with_precision(self, address: StateAddress) -> Optional[float]:
        """

        :param StateAddress address: address from which to read value
        :return: value if value exceeds precision and None if it is not
        """
        value = await self._states[address].get_value()
        if abs(self._values.get_value(address) - value) >= self._precision:
            self._values.set_value(address, value)
            return value

    async def read_all_values(self) -> Iterator[Tuple[StateAddress, float]]:
        """
        Scans onewire bus and retrieves values from all states

        :return: dict of key: state address and value: float
        """
        return zip(
            self._states.keys(),
            await asyncio.gather(*[self.read_value(address) for address in self._states])
        )

    async def read_all_values_with_precision(self) -> Iterator[Tuple[StateAddress, float]]:
        """
        Scans onewire bus and retrieves values from all states

        :return: dict of key: state address and value: float
        """
        return filter(
            lambda x: x[1],
            zip(
                self._states.keys(),
                await asyncio.gather(*[self.read_value_with_precision(address) for address in self._states])
            )
        )

    async def subscribe(self) -> AsyncIterator[Tuple[StateAddress, float]]:
        """
        Subscribes to onewire bus and asynchronously yields results
        if any of states changed by value bigger than configured precision

        :return: iterator of tuples (address, value)
        """
        for address, value in await self.read_all_values():
            yield address, value
        while True:
            values = await self.read_all_values_with_precision()
            for address, value in values:
                yield address, value

    def get_value(self, address: StateAddress) -> float:
        """
        Gets most recent value from state without reading it from bus

        :param StateAddress address: addres from which to get value
        :return: cached value of state
        """
        return self._values.get_value(address)
