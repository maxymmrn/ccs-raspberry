from ..state.address import StateAddress


class StateValuesMap:

    def __init__(self, init_data: dict = None):
        self._values = {} if init_data is None else init_data

    def get_value(self, address: StateAddress):
        return self._values[address.path]

    def set_value(self, address: StateAddress, value):
        self._values[address.path] = value

    def items(self):
        return self._values.items()

    def __str__(self):
        return ',\n'.join([f'{key}: {value}' for key, value in self._values.items()])
